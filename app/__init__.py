"""
Autor : Alexis Calderón
Fecha: 6 may 2022
"""
import logging
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()


def create_app(settings_module):
	app = Flask(__name__, instance_relative_config=True)
	app.config.from_object(settings_module)
	# Load the configuration from the instance folder
	if app.config.get('TESTING', False):
		app.config.from_pyfile('config-testing.py', silent=True)
	else:
		app.config.from_pyfile('config.py', silent=True)

	db.init_app(app)
	app.app_context().push()
	
	migrate.init_app(app, db)

	from .libro import libro_bp
	app.register_blueprint(libro_bp)
	
	# Custom error handlers
	register_error_handlers(app)

	return app



def register_error_handlers(app):

	# redireccionar paginas de error
	@app.errorhandler(404)
	def page_not_found(e):
	# note that we set the 404 status explicitly
		return render_template('404.html'), 404

	@app.errorhandler(401)
	def page_not_found(e):
	# note that we set the 404 status explicitly
		return render_template('401.html'), 401


def configure_logging(app):
	"""
	Configura el módulo de logs. Establece los manejadores para cada logger.

	:param app: Instancia de la aplicación Flask

	"""

	# Elimina los manejadores por defecto de la app
	del app.logger.handlers[:]

	loggers = [app.logger, ]
	handlers = []

	console_handler = logging.StreamHandler()
	console_handler.setFormatter(verbose_formatter())
	if (app.config['APP_ENV'] == app.config['APP_ENV_TESTING']) or (
		app.config['APP_ENV'] == app.config['APP_ENV_DEVELOPMENT']):
		console_handler.setLevel(logging.DEBUG)
		handlers.append(console_handler)
	elif app.config['APP_ENV'] == app.config['APP_ENV_PRODUCTION']:
		console_handler.setLevel(logging.INFO)
		handlers.append(console_handler)

		for l in loggers:
			for handler in handlers:
				l.addHandler(handler)
			l.propagate = False
			l.setLevel(logging.DEBUG)

	gunicorn_error_logger = logging.getLogger('gunicorn.error')
	app.logger.handlers.extend(gunicorn_error_logger.handlers)

def verbose_formatter():
	return logging.Formatter(
	'[%(asctime)s.%(msecs)d]\t %(levelname)s \t[%(name)s.%(funcName)s:%(lineno)d]\t %(message)s',
	datefmt='%d/%m/%Y %H:%M:%S'
	)
