"""
Autor : Alexis Calderón
Fecha: 6 may 2022
"""
from flask import Flask, request, redirect, url_for, abort,  Response

from . import libro_bp
from .model import Libro
from .helper import LibroApiExterno
import json

WHITE_LIST_ARGS = ['titulo', 'subtitulo', 'autor', 'categoria', 'fecha_publicacion', 'editor', 'descripcion']



@libro_bp.route("/libros/", methods=["GET"])
def libros():

	args = {}
	for arg in WHITE_LIST_ARGS:
		arg_ = request.args.get(arg)
		if arg_ != None:
			args[arg] = arg_
	libros = Libro.buscar(args)
	if len(libros['libros']) == 0:
		libros = LibroApiExterno.consultaApiGoogle(args)

	return Response(str(libros))


@libro_bp.route("/libros/", methods=["POST"])
def agregarLibro():
	data = request.data.decode('utf-8')
	print(data)
	arg = json.loads(data)
	if arg != None:
		libro = LibroApiExterno.consultaApiGoogle(arg)
		if libro:
			Libro.new(libro)
		return libro



@libro_bp.route("/librosApiExterno2/", methods=["GET"])
def librosOpenLibra():

	args = {}
	for arg in WHITE_LIST_ARGS:
		arg_ = request.args.get(arg)
		if arg_ != None:
			args[arg] = arg_
	
	libros = LibroApiExterno.consultaApiOpenLibra(args)

	return Response(str(libros))


@libro_bp.route("/libros/<string:id_libro_a_aliminar>/delete", methods=["DELETE"])
def delete(id_libro_a_aliminar):
	libro = Libro.get_by_id(id_libro_a_aliminar)
	Libro.delete(libro)

	return Response("solicitud realizada")
