"""
Autor : Alexis Calderón
Fecha: 6 may 2022
"""
from flask import Blueprint

libro_bp = Blueprint('libro', __name__, template_folder='templates')

from . import routes