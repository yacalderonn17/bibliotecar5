"""
Autor : Alexis Calderón
Fecha: 6 may 2022
"""

from app import db
from sqlalchemy import select

class Libro(db.Model):

	id = db.Column(db.Integer, primary_key=True)
	titulo = db.Column(db.String(50))
	subtitulo = db.Column(db.String(50))
	autor = db.Column(db.String(50))
	categoria = db.Column(db.String(50))
	fecha_publicacion = db.Column(db.String(10))
	editor =  db.Column(db.String(20))
	descripcion =  db.Column(db.String(2000))
	imagen =  db.Column(db.String(50000)) # se podría guardar imagen en base 64, no es optimo

	def __init__(self, titulo, subtitulo, autor, categoria, fecha_publicacion, editor, descripcion, imagen):
		self.titulo = titulo
		self.subtitulo = subtitulo 
		self.autor = autor 
		self.categoria = categoria 
		self.fecha_publicacion = fecha_publicacion 
		self.editor = editor 
		self.descripcion = descripcion 
		self.imagen = imagen 
		
	def __repr__(self):
		return f'<titulo {self.titulo}>'


	def save(self):
		if not self.id:
			db.session.add(self)
		db.session.commit()
		return True

	def delete(self):
		db.session.delete(self)
		db.session.commit()

	@staticmethod
	def get_by_id(id):
		return Libro.query.get(id)

	
	@staticmethod
	def get_all():
		return Libro.query.all()

	@staticmethod
	def new(datosConsulta):
		try:
			libro = Libro(datosConsulta['items']['volumeInfo']['title'],
				'',datosConsulta['items']['volumeInfo']['autor'][0],
				datosConsulta['items']['volumeInfo']['categories'][0],
				datosConsulta['items']['volumeInfo']['publishedDate'],
				datosConsulta['items']['volumeInfo']['publisher'],
				datosConsulta['items']['volumeInfo']['descripcion']		
				)
			
			libro.save()
			return True
		except:
			return False

	@staticmethod
	def buscar(parametrosDeBusqueda):
		ans = []
		libros =  Libro.query
		
		if 'titulo' in parametrosDeBusqueda:
			libros = libros.filter_by(titulo=parametrosDeBusqueda['titulo'])
		if 'subtitulo' in parametrosDeBusqueda:
			libros = libros.filter_by(subtitulo=parametrosDeBusqueda['subtitulo'])
		if 'autor' in parametrosDeBusqueda:
			libros = libros.filter_by(autor=parametrosDeBusqueda['autor'])
		if 'categoria' in parametrosDeBusqueda:
			libros = libros.filter_by(categoria=parametrosDeBusqueda['categoria'])
		if 'fecha_publicacion' in parametrosDeBusqueda:
			libros = libros.filter_by(fecha_publicacion=parametrosDeBusqueda['fecha_publicacion'])
		if 'editor' in parametrosDeBusqueda:
			libros = libros.filter_by(editor=parametrosDeBusqueda['editor'])
		if 'descripcion' in parametrosDeBusqueda:
			libros = libros.filter_by(descripcion=parametrosDeBusqueda['descripcion'])


		for libro in libros.all():
			ans.append(libro.__dict__)

		return {'fuente': 'db interna', 'libros': ans}
