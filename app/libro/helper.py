"""
Autor : Alexis Calderón
Fecha: 6 may 2022
"""
import requests
import json

API_GOOGLE_URL = "https://www.googleapis.com/books/v1/volumes"
API_OPENLIBRA_URL = "https://www.googleapis.com/books/v1/volumes"

class LibroApiExterno(object):

	
	@staticmethod
	def consultaApiGoogle(parametrosDeBusqueda):

		
		if 'titulo' in parametrosDeBusqueda:
			params = dict(q=parametrosDeBusqueda['titulo'])
			params['intitle'] = parametrosDeBusqueda['titulo']
		else:
			params = dict(q='libro')
		if 'autor' in parametrosDeBusqueda:
			params['inauthor'] = parametrosDeBusqueda['autor']
		if 'editor' in parametrosDeBusqueda:
			params['inpublisher'] = parametrosDeBusqueda['editor']
		if 'isnb' in parametrosDeBusqueda:
			params['isnb'] = parametrosDeBusqueda['isnb']

		resp = requests.get(API_GOOGLE_URL, params=params)
		resp = json.loads(resp.content)
		resp['fuente'] = "google"
		return resp


	@staticmethod
	def consultaApiOpenLibra(parametrosDeBusqueda):

		params = dict(q=None)
		if 'titulo' in parametrosDeBusqueda:
			params['book_title'] = parametrosDeBusqueda['titulo']
		if 'categoria' in parametrosDeBusqueda:
			params['category'] = parametrosDeBusqueda['categoria']
		if 'autor' in parametrosDeBusqueda:
			params['book_author'] = parametrosDeBusqueda['autor']
		if 'fecha_publicacion' in parametrosDeBusqueda:
			params['publisher_date'] = parametrosDeBusqueda['fecha_publicacion']
		if 'editor' in parametrosDeBusqueda:
			params['publishers'] = parametrosDeBusqueda['editor']
		
		resp = requests.get(API_OPENLIBRA_URL, params=params)
		return json.loads(resp.content)

