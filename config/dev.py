"""
Autor : Alexis Calderón
Fecha: 6 may 2022
"""
from .default import *

APP_ENV = APP_ENV_DEVELOPMENT
DEBUG = True
SQLALCHEMY_DATABASE_URI = 'sqlite:////home/alexis/LibreriaR5.db'
